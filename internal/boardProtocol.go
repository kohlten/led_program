package internal

import (
	"bytes"
	"fmt"
	"github.com/pkg/errors"
	"github.com/tarm/serial"
	"time"
)

type BoardProtocol struct {
	conn    *serial.Port
	locked  bool
	canSend bool
}

func NewBoardProtocol(file string, baud uint64, timeout uint64) (*BoardProtocol, error) {
	board := new(BoardProtocol)
	c := serial.Config{
		Name:        file,
		Baud:        int(baud),
		ReadTimeout: time.Duration(timeout * uint64(time.Millisecond)),
	}
	conn, err := serial.OpenPort(&c)
	if err != nil {
		return nil, err
	}
	board.conn = conn
	board.canSend = true
	return board, nil
}

func (b *BoardProtocol) Send(strips []*Strip) {
	if b.locked {
		return
	}
	b.locked = true
	if !b.canSend {
		data := make([]byte, 2)
		for {
			n, _ := b.conn.Read(data)
			if n > 0 {
				fmt.Println(data)
			}
			if n > 0 && bytes.Compare(data, []byte("\r\n")) == 0 {
				b.canSend = true
				break
			}
		}
	}
	data := make([]byte, 0)
	for i := 0; i < len(strips); i++ {
		data = append(data, strips[i].toData()...)
	}
	_, err := b.conn.Write(data)
	b.canSend = false
	b.locked = false
	if err != nil {
		panic(errors.Wrap(err, "Failed to write to board with error: "))
	}
}

func (b *BoardProtocol) Close() {
	_ = b.conn.Close()
}
