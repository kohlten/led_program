package internal

import (
	"github.com/inkyblackness/imgui-go"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/kohlten/glLib"
	imguiImpl "gitlab.com/kohlten/led_program/internal/imgui-impl"
	"runtime"
	"sort"
)

func init() {
	runtime.LockOSThread()
}

var BackButtonPos imgui.Vec2
var BackButtonSize = imgui.Vec2{X: 100, Y: 20}

type WindowManager struct {
	window   *glLib.Window
	platform *imguiImpl.SDL
	renderer *imguiImpl.OpenGL3
	ctx      *imgui.Context
	io       imgui.IO
	running  bool
}

func NewWindowManager() (*WindowManager, error) {
	settings := glLib.NewWindowSettings()
	settings.Name = "Led Manager"
	settings.Width = 500
	settings.Height = 500
	settings.OpenGlVersionMajor = 3
	settings.OpenGlVersionMinor = 2
	settings.UseGl = true
	settings.Vsync = true
	window, err := glLib.NewWindow(settings)
	if err != nil {
		return nil, err
	}
	ctx := imgui.CreateContext(nil)
	io := imgui.CurrentIO()
	platform := imguiImpl.NewSDL(io, window.Window)
	renderer := imguiImpl.NewOpenGL3(io)
	renderer.CreateFontsTexture()
	BackButtonPos.Y = float32(settings.Height) - (BackButtonSize.Y * 2)
	return &WindowManager{
		window:   window,
		platform: platform,
		renderer: renderer,
		ctx:      ctx,
		io:       io,
		running:  true,
	}, nil
}

func (wm *WindowManager) input() {
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		wm.platform.ProcessEvent(event)
		switch event.(type) {
		case *sdl.QuitEvent:
			wm.running = false
		}
	}
}

func (wm *WindowManager) update() {

}

func (wm *WindowManager) draw(strips []*Strip, moduleManager *ModuleManager) {
	wm.platform.NewFrame()
	imgui.NewFrame()
	wm.renderer.PreRender([3]float32{})
	current := moduleManager.GetCurrent()
	if current != nil {
		current.Run(strips)
		wm.drawBack(strips, moduleManager)
	} else {
		wm.drawModuleMenu(strips, moduleManager)
	}
	imgui.Render()
	wm.renderer.Render(wm.platform.DisplaySize(), wm.platform.FramebufferSize(), imgui.RenderedDrawData())
	wm.window.Window.GLSwap()
}

func (wm *WindowManager) Run(strips []*Strip, moduleManager *ModuleManager) {
	wm.input()
	wm.update()
	wm.draw(strips, moduleManager)
}

func (wm *WindowManager) IsRunning() bool {
	return wm.running
}

func (wm *WindowManager) drawModuleMenu(strips []*Strip, moduleManager *ModuleManager) {
	imgui.SetNextWindowPosV(imgui.Vec2{}, imgui.ConditionAlways, imgui.Vec2{})
	size := wm.platform.DisplaySize()
	imgui.SetNextWindowSizeV(imgui.Vec2{X: size[0], Y: size[1]}, imgui.ConditionAlways)
	imgui.BeginV("moduleSelect", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize)
	names := moduleManager.GetAllModuleNames()
	sort.Strings(names)
	for _, k := range names {
		if imgui.SelectableV(k, false, imgui.SelectableFlagsAllowDoubleClick, imgui.Vec2{}) {
			if moduleManager.SetModule(k) {
				moduleManager.GetCurrent().Init(strips)
			}
		}
	}
	imgui.End()
}

func (wm *WindowManager) drawBack(strips []*Strip, moduleManager *ModuleManager) {
	imgui.SetNextWindowPos(BackButtonPos)
	imgui.BeginV("back", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize|imgui.WindowFlagsNoBackground)
	if imgui.ButtonV("Back", BackButtonSize) {
		moduleManager.ResetModule()
		for i := 0; i < len(strips); i++ {
			strips[i].SetAll(Pixel{})
		}
	}
	imgui.End()
}
