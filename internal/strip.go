package internal

type Strip struct {
	data        []Pixel
	size        uint64
	currentSize uint64
	pin         uint16
}

// Pin is not used at the moment, going to be needed when you can add or remove strips at any time.
func NewStrip(pin uint16, size uint64) *Strip {
	return &Strip{pin: pin, data: make([]Pixel, size), size: size}
}

func (s *Strip) Add(pixel Pixel) {
	if s.currentSize >= s.size {
		return
	}
	s.data[s.currentSize] = pixel
	s.currentSize += 1
}

func (s *Strip) Len() uint64 {
	return s.size
}

func (s *Strip) Set(loc int, pixel Pixel) {
	if loc < 0 || uint64(loc) >= s.size {
		return
	}
	s.data[loc] = pixel
}

func (s *Strip) SetAll(pixel Pixel) {
	for i := uint64(0); i < s.size; i++ {
		s.data[i] = pixel
	}
}

func (s *Strip) Reset() {
	for i := uint64(0); i < s.size; i++ {
		s.data[i] = Pixel{}
	}
}

func (s *Strip) toData() []byte {
	data := make([]byte, s.size*3)
	current := 0
	for i := uint64(0); i < s.size*3; i += 3 {
		data[i+0] = s.data[current].R
		data[i+1] = s.data[current].G
		data[i+2] = s.data[current].B
		current += 1
	}
	return data
}
