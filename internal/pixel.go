package internal

type Pixel struct {
	R byte
	G byte
	B byte
}

func NewPixel(r, g, b byte) Pixel {
	return Pixel{
		R: r,
		G: g,
		B: b,
	}
}
