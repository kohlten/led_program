package internal

import (
	"io/ioutil"
	"os"
	"path/filepath"
)

type Manager struct {
	boardProto    *BoardProtocol
	moduleManager *ModuleManager
	strips        []*Strip
	windowManager *WindowManager
}

func NewManager(file string, baud uint64, timeout uint64) (*Manager, error) {
	manager := new(Manager)
	manager.moduleManager = NewModuleManager()
	board, err := NewBoardProtocol(file, baud, timeout)
	if err != nil {
		return nil, err
	}
	manager.boardProto = board
	err = manager.initModules()
	if err != nil {
		return nil, err
	}
	windowManager, err := NewWindowManager()
	if err != nil {
		return nil, err
	}
	manager.windowManager = windowManager
	return manager, nil
}

func (m *Manager) AddStrip(strip *Strip) {
	m.strips = append(m.strips, strip)
}

func (m *Manager) GetStripsLen() int {
	return len(m.strips)
}

func (m *Manager) Update() {
	// Update current script
	// Send to board
	m.windowManager.Run(m.strips, m.moduleManager)
	go m.boardProto.Send(m.strips)
}

func (m *Manager) GetStrip(loc int) *Strip {
	if loc < 0 || loc >= len(m.strips) {
		return nil
	}
	return m.strips[loc]
}

func (m *Manager) Close() {
	m.boardProto.Close()
}

func (m *Manager) IsRunning() bool {
	return m.windowManager.IsRunning()
}

func (m *Manager) initModules() error {
	moduleNames, err := m.listModules()
	if err != nil {
		return err
	}
	for _, name := range moduleNames {
		err := m.moduleManager.AddModule(name)
		if err != nil {
			return err
		}
	}
	return nil
}

func (m *Manager) listModules() ([]string, error) {
	base := os.Args[0]
	base = filepath.Dir(base)
	files, err := ioutil.ReadDir(base + "/../modules/")
	if err != nil {
		return nil, err
	}
	fileLoc := make([]string, 0)
	for _, file := range files {
		if file.IsDir() {
			fileLoc = append(fileLoc, base+"/../modules/"+file.Name())
		}
	}
	return fileLoc, nil
}
