package internal

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"plugin"
)

type GuiFunc func(data map[string]interface{})
type RunFunc func(strips []*Strip, data map[string]interface{})
type InitFunc func(strips []*Strip, data map[string]interface{})
type FreeFunc func()

type Module struct {
	name      string
	variables map[string]interface{}
	guiFunc   GuiFunc
	runFunc   RunFunc
	initFunc  InitFunc
	freeFunc  FreeFunc
}

func NewModule(loc string) (*Module, error) {
	if stat, err := os.Stat(loc); err != nil || !stat.IsDir() {
		return nil, fmt.Errorf("%s is not a directory", loc)
	}
	m := &Module{name: filepath.Base(loc)}
	err := m.loadModule(loc)
	if err != nil {
		return nil, err
	}
	return m, nil
}

func (m *Module) loadModule(base string) error {
	p, err := plugin.Open(base + "/" + filepath.Base(base) + ".so")
	if err != nil {
		return err
	}
	guiFuncInterface, err := p.Lookup("RunGui")
	if err != nil {
		return err
	}
	guiFunc, ok := guiFuncInterface.(func(map[string]interface{}))
	if !ok {
		return fmt.Errorf("GuiFunc in module %s does not have the defition func()", m.name)
	}
	m.guiFunc = guiFunc
	runFuncInterface, err := p.Lookup("Run")
	if err != nil {
		return err
	}
	runFunc, ok := runFuncInterface.(func([]*Strip, map[string]interface{}))
	if !ok {
		return fmt.Errorf("RunFunc in module %s does not have the defition func([]*Strip)", m.name)
	}
	m.runFunc = runFunc
	initFuncInterface, err := p.Lookup("Init")
	if err != nil {
		return err
	}
	initFunc, ok := initFuncInterface.(func([]*Strip, map[string]interface{}))
	if !ok {
		return fmt.Errorf("InitFunc in module %s does not have the defition func([]*Strip)", m.name)
	}
	m.initFunc = initFunc
	freeFuncInterface, err := p.Lookup("Free")
	if err != nil {
		return err
	}
	freeFunc, ok := freeFuncInterface.(func())
	if !ok {
		return fmt.Errorf("FreeFunc in module %s does not have the defition func()", m.name)
	}
	m.freeFunc = freeFunc
	m.variables = make(map[string]interface{})
	data, jsonErr := ioutil.ReadFile(base + "/variables.json")
	if jsonErr == nil {
		jsonErr = json.Unmarshal(data, &m.variables)
		if jsonErr != nil {
			fmt.Println("WARNING: Failed to parse variable json in module", m.name, " with error", jsonErr.Error())
		}
	}
	return err
}

func (m *Module) Run(strips []*Strip) {
	m.guiFunc(m.variables)
	m.runFunc(strips, m.variables)
}

func (m *Module) Init(strips []*Strip) {
	m.initFunc(strips, m.variables)
}

func (m *Module) Free() {
	m.freeFunc()
}

func (m *Module) GetName() string {
	return m.name
}
