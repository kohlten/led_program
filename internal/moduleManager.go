package internal

type ModuleManager struct {
	modules map[string]*Module
	current *Module
}

func NewModuleManager() *ModuleManager {
	return &ModuleManager{modules: make(map[string]*Module)}
}

func (m *ModuleManager) AddModule(loc string) error {
	module, err := NewModule(loc)
	if err != nil {
		return err
	}
	m.modules[module.GetName()] = module
	return nil
}

func (m *ModuleManager) RemoveModule(name string) bool {
	_, ok := m.modules[name]
	if ok {
		delete(m.modules, name)
	}
	return ok
}

func (m *ModuleManager) SetModule(name string) bool {
	module, ok := m.modules[name]
	if ok {
		m.current = module
	}
	return ok
}

func (m *ModuleManager) ResetModule() {
	if m.current != nil {
		m.current.Free()
		m.current = nil
	}
}

func (m *ModuleManager) GetCurrent() *Module {
	return m.current
}

func (m *ModuleManager) GetAllModules() map[string]*Module {
	return m.modules
}

func (m *ModuleManager) GetAllModuleNames() []string {
	output := make([]string, len(m.modules))
	i := 0
	for k := range m.modules {
		output[i] = k
		i += 1
	}
	return output
}
