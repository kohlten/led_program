package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
)

var gcflags = flag.String("gcflags", "", "GCC flags to add to each compiled module")

func main() {
	fmt.Println(os.Args)
	flag.Parse()
	fmt.Println(*gcflags)
	names, err := listModules()
	if err != nil {
		panic(fmt.Sprint("Failed to load modules with error:", err))
	}
	Compile(names, *gcflags)
}

func Compile(names []string, flags string) {
	for _, v := range names {
		CompileOne(v, flags)
	}
}

func CompileOne(name string, flags string) {
	cmd := exec.Command("go", "build", "-buildmode=plugin", "-gcflags", flags, filepath.Base(name))
	cmd.Dir = filepath.Dir(name)
	err := cmd.Run()
	output, _ := cmd.Output()
	if err != nil {
		fmt.Println("Failed to compile module ", name, ". It will not be included. go output: ", string(output))
		return
	}
}

func listModules() ([]string, error) {
	base := os.Args[0]
	base = filepath.Dir(base)
	files, err := ioutil.ReadDir(base + "/../modules")
	if err != nil {
		return nil, err
	}
	fileLoc := make([]string, 0)
	for _, file := range files {
		if file.IsDir() {
			fileLoc = append(fileLoc, base+"/../modules/"+file.Name()+"/"+file.Name()+".go")
		}
	}
	return fileLoc, nil
}
