package main

import (
	"github.com/inkyblackness/imgui-go"
	"github.com/lucasb-eyer/go-colorful"
	"gitlab.com/kohlten/led_program/internal"
	"math/rand"
)

var hue float32
var saturation float32
var fadeTime float32 = 10
var currentFade float32
var step float32
var goingUp bool
var maxBrightness float32

func Init(strips []*internal.Strip, data map[string]interface{}) {}

func RunGui(data map[string]interface{}) {
	imgui.SetNextWindowPos(imgui.Vec2{})
	imgui.BeginV("colorSelect", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize|imgui.WindowFlagsAlwaysAutoResize)
	imgui.SliderFloat("FadeTime", &fadeTime, 1, 5000)
	imgui.SliderFloat("MaxBrightness", &maxBrightness, 0.001, 1)
	imgui.End()
}

func Run(strips []*internal.Strip, data map[string]interface{}) {
	if goingUp && currentFade >= maxBrightness {
		goingUp = !goingUp
	}
	if !goingUp && currentFade <= 0 {
		hue = float32(rand.Intn(360))
		saturation = rand.Float32()
		goingUp = !goingUp
		if goingUp {
			currentFade = 0
		} else {
			currentFade = maxBrightness
		}
		if fadeTime == 0 {
			fadeTime = 1
		}
	}
	step = maxBrightness / fadeTime
	if goingUp {
		currentFade += step
	} else {
		currentFade -= step
	}
	if currentFade < 0 {
		currentFade = 0
	}
	set(strips)
}

func set(strips []*internal.Strip) {
	color := colorful.Hsl(float64(hue), float64(saturation), float64(currentFade))
	for i := 0; i < len(strips); i++ {
		for j := uint64(0); j < strips[i].Len(); j++ {
			strips[i].Set(int(j), internal.Pixel{
				R: byte(color.R * 255),
				G: byte(color.G * 255),
				B: byte(color.B * 255),
			})
		}
	}
}

func Free() {}