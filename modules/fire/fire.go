package main

import (
	"github.com/inkyblackness/imgui-go"
	"gitlab.com/kohlten/led_program/internal"
	"math/rand"
	"time"
)

// Fire2012 by Mark Kriegsman, July 2012
// as part of "Five Elements" shown here: http://youtu.be/knWiGsmgycY
////
// This basic one-dimensional 'fire' simulation works roughly as follows:
// There's a underlying array of 'heat' cells, that model the temperature
// at each point along the line.  Every cycle through the simulation,
// four steps are performed:
//  1) All cells cool down a little bit, losing heat to the air
//  2) The heat from each cell drifts 'up' and diffuses a little
//  3) Sometimes randomly new 'sparks' of heat are added at the bottom
//  4) The heat from each cell is rendered as a color into the leds array
//     The heat-to-color mapping uses a black-body radiation approximation.
//
// Temperature is in arbitrary units from 0 (cold black) to 255 (white hot).
//
// This simulation scales it self a bit depending on NUM_LEDS; it should look
// "OK" on anywhere from 20 to 100 LEDs without too much tweaking.
//
// I recommend running this simulation at anywhere from 30-100 frames per second,
// meaning an interframe delay of about 10-35 milliseconds.
//
// Looks best on a high-density LED setup (60+ pixels/meter).
//
//
// There are two main parameters you can play with to control the look and
// feel of your fire: COOLING (used in step 1 above), and SPARKING (used
// in step 3 above).
//
// COOLING: How much does the air cool as it rises?
// Less cooling = taller flames.  More cooling = shorter flames.
// Default 50, suggested range 20-100
var cooling = int32(55)

// SPARKING: What chance (out of 255) is there that a new spark will be lit?
// Higher chance = more roaring fire.  Lower chance = more flickery fire.
// Default 120, suggested range 50-200.
var sparking = int32(120)

var reverse bool

var heat []byte

func Init(strips []*internal.Strip, data map[string]interface{}) {
	rand.Seed(time.Now().UnixNano())
	if len(strips) == 0 {
		return
	}
	max := strips[0].Len()
	for i := 1; i < len(strips); i++ {
		if strips[i].Len() > max {
			max = strips[i].Len()
		}
	}
	heat = make([]byte, max)
}

func RunGui(data map[string]interface{}) {
	imgui.SetNextWindowPos(imgui.Vec2{})
	imgui.BeginV("fire", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize|imgui.WindowFlagsAlwaysAutoResize)
	imgui.SliderInt("cooling", &cooling, 1, 255)
	imgui.SliderInt("sparking", &sparking, 1, 255)
	imgui.Checkbox("reverse", &reverse)
	imgui.End()
}

func Run(strips []*internal.Strip, data map[string]interface{}) {
	for i := 0; i < len(heat); i++ {
		reduction := rand.Intn(((int(cooling) * 10) / len(heat)) + 2)
		if int(heat[i]) - reduction >= 0 {
			heat[i] -= byte(reduction)
		}
	}
	for i := len(heat) - 1; i >= 2; i-- {
		heat[i] = (heat[i - 1] + heat[i - 2] + heat[i - 2]) / 3
	}
	if rand.Intn(255) < int(sparking) {
		loc := rand.Intn(7)
		heat[loc] += byte(rand.Intn(95) + 160)
	}
	for i := 0; i < len(heat); i++ {
		color := heatColor(heat[i])
		loc := 0
		if reverse {
			loc = (len(heat) - 1) - i
		} else {
			loc = i
		}
		for j := 0; j < len(strips); j++ {
			strips[j].Set(loc, color)
		}
	}

}

func heatColor(val byte) internal.Pixel {
	color := internal.Pixel{}
	scaled := byte(scale(float32(val), 0, 255, 0, 191))
	ramp := scaled & 0x3F
	ramp <<= 2
	if scaled & 0x80 > 0 {
		color.R = 255
		color.G = 255
		color.B = ramp
	} else if scaled & 0x40 > 0 {
		color.R = 255
		color.G = ramp
	} else {
		color.R = ramp
	}
	return color
}

func scale(num, inMin, inMax, outMin, outMax float32) float32 {
	return (outMax - outMin) * (num - inMin) / (inMax - inMin) + outMin
}

func Free() {}