package main

import (
	"fmt"
	"github.com/inkyblackness/imgui-go"
	"github.com/lucasb-eyer/go-colorful"
	"gitlab.com/kohlten/freq_graph/audio"
	"gitlab.com/kohlten/led_program/internal"
	"strconv"
)

var parser *audio.Parser
var current []float64
var hasFirst bool
var colors [][3]float32
var isRGB []bool

// @TODO Make this so it will fade out from the top led that is lit
// @TODO Change the sink to a variable from data
func Init(strips []*internal.Strip, data map[string]interface{}) {
	if parser == nil {
		sinkInterface, _ := data["sink"]
		sink, _ := sinkInterface.(string)
		p, err := audio.NewParser(sink, 512, 8000, 1, 512)
		if err != nil {
			panic(fmt.Sprintf("Error creating parser: %s", err))
		}
		parser = p
	}
	colors = make([][3]float32, len(strips))
	isRGB = make([]bool, len(strips))
}

func RunGui(data map[string]interface{}) {
	imgui.SetNextWindowPos(imgui.Vec2{})
	imgui.BeginV("fft", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize|imgui.WindowFlagsAlwaysAutoResize)
	for i := 0; i < len(colors); i++ {
		loc := strconv.FormatUint(uint64(i), 10)
		imgui.Checkbox("Is RGB " + loc, &isRGB[i])
		if !isRGB[i] {
			imgui.SliderFloat("Hue " + loc , &colors[i][0], 0, 360)
			imgui.SliderFloat("Saturation " + loc, &colors[i][1], 0, 1.0)
			imgui.SliderFloat("Brightness " + loc, &colors[i][2], 0, 1.0)
		} else {
			imgui.SliderFloat3("RGB " + loc , &colors[i], 0.0, 1.0)
		}
	}
	imgui.End()
}

func Run(strips []*internal.Strip, data map[string]interface{}) {
	if parser.IsReady() {
		current = parser.GetData()
		hasFirst = true
	}
	if hasFirst {
		highest := audio.Highest(current, 3)
		audio.Range(highest, 0, 200, 0, 144)
		for i := 0; i < len(strips); i++ {
			setUpTo(byte(highest[i]), strips[i], i)
		}
	}
}

func setUpTo(val byte, strip *internal.Strip, loc int) {
	colorOn := getColor(loc)
	colorOff := internal.NewPixel(0, 0, 0)
	for i := byte(0); i < val; i++ {
		strip.Set(int(i), colorOn)
	}
	for i := uint64(val); i < strip.Len(); i++ {
		strip.Set(int(i), colorOff)
	}
}

func getColor(loc int) internal.Pixel {
	if isRGB[loc] {
		return internal.NewPixel(byte(colors[loc][0] * 255), byte(colors[loc][0] * 255), byte(colors[loc][0] * 255))
	} else {
		rgbColor := colorful.Hsl(float64(colors[loc][0]), float64(colors[loc][1]), float64(colors[loc][2]))
		return internal.NewPixel(byte(rgbColor.R * 255), byte(rgbColor.G * 255), byte(rgbColor.B * 255))
	}
}

func Free() {
	parser.Close()
	parser = nil
}
