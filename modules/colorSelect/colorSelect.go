package main

import (
	"github.com/inkyblackness/imgui-go"
	"github.com/lucasb-eyer/go-colorful"
	"gitlab.com/kohlten/led_program/internal"
)

var color [3]float32
var isRgb bool

func Init(strips []*internal.Strip, data map[string]interface{}) {}

func RunGui(data map[string]interface{}) {
	imgui.SetNextWindowPos(imgui.Vec2{})
	imgui.BeginV("colorSelect", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize|imgui.WindowFlagsAlwaysAutoResize)
	imgui.Checkbox("Is RGB", &isRgb)
	if !isRgb {
		imgui.SliderFloat("Hue", &color[0], 0, 360)
		imgui.SliderFloat("Saturation", &color[1], 0, 1.0)
		imgui.SliderFloat("Brightness", &color[2], 0, 1.0)
	} else {
		imgui.SliderFloat3("RGB", &color, 0.0, 1.0)
	}
	imgui.End()
}

func Run(strips []*internal.Strip, data map[string]interface{}) {
	rgbColor := colorful.Color{}
	if !isRgb {
		rgbColor = colorful.Hsl(float64(color[0]), float64(color[1]), float64(color[2]))
	} else {
		rgbColor.R = float64(color[0])
		rgbColor.G = float64(color[1])
		rgbColor.B = float64(color[2])
	}
	for i := 0; i < len(strips); i++ {
		for j := uint64(0); j < strips[i].Len(); j++ {
			strips[i].Set(int(j), internal.NewPixel(byte(rgbColor.R*255), byte(rgbColor.G*255), byte(rgbColor.B*255)))
		}
	}
}

func Free() {
	color[0] = 0
	color[1] = 0
	color[2] = 0
	isRgb = false
}
