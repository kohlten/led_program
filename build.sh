mkdir bin
cd module_compiler && go build && mv module_compiler ../bin && cd ..
go build && mv led_program bin
FLAGS=""
#For debug building, uncomment for using
FLAGS="\"all=-N -l\""
echo $FLAGS
cd bin && ./module_compiler $FLAGS