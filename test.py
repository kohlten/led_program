import time
import random
import serial


def make_data(color):
    data = ""
    for i in range(432):
        data += chr(color[0])
        data += chr(color[1])
        data += chr(color[2])
    return data


def make_random(max):
    data = ""
    for i in range(432):
        data += chr(random.randint(0, max))
        data += chr(random.randint(0, max))
        data += chr(random.randint(0, max))
    return data


def snake(loc, size, base_color, snake_color):
    data = ""
    current = loc - size
    if current < 0:
        current = 432 - (current * -1)
    set_size = 0
    for i in range(432):
        if i == current:
            data += chr(snake_color[0])
            data += chr(snake_color[1])
            data += chr(snake_color[2])
            if set_size < size:
                current += 1
            set_size += 1
            if current >= 432:
                current = 0
        else:
            data += chr(base_color[0])
            data += chr(base_color[1])
            data += chr(base_color[2])

    return data


def slow_write(conn, data, delay):
    for i in range(0, len(data)):
        conn.write(data[i])
        time.sleep(delay)


def slow_write_multi(conn, data, rate, delay):
    for i in range(0, len(data), rate):
        conn.write(data[i:i + rate])
        time.sleep(delay)


def main():
    #0.0015

    conn = serial.Serial('/dev/ttyACM0', 2000000, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE, None, True)
    snake_loc = 400
    snake_size = 30
    ##st = time.time()
    #slow_write_multi(conn, data, 3, 0.00407)
    iter = 0
    while 1:
        #color = [random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)]
        color = [255, 255, 255]
        data = make_data(color)
        conn.write(data)
        while True:
            returnData = conn.read_all()
            if returnData == "START\n":
                break
        #conn.write(data)
        #color = [255, 255, 255]
        #color = [random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)]
        #data = snake(snake_loc, snake_size, [0, 100, 0], [100, 0, 0])
        #st = time.time()
        #conn.write(data)
        #conn.read_until("START\n")
        print("UPDATED " + str(iter))
        iter += 1
        #print(time.time() - st)
        #time.sleep(0.01)
        #snake_loc += 5
        #if snake_loc > 432:
        #    snake_loc = 0

        #color = [0, 0, 0]
        #data = make_data(color)
        #conn.write(data)
        #time.sleep(0.02)

    #slow_write(conn, data, 0.0015)
    #print(conn.read_until("SET"))
    ##while 1:
        ##print(conn.read_all())
    ##conn.read_until("SET\n")
    ##print(time.time() - st)
    #slow_write(conn, data, 0.0011)
    #while 1:

     #   slow_write(conn, data, 0.001)
      #  data = make_data(1, color)
       # slow_write(conn, data, 0.001)
        #data = make_data(2, color)
        #slow_write(conn, data, 0.001)
    conn.close()


if __name__ == '__main__':
    main()