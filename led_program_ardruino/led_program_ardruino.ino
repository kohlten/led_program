#define FASTLED_INTERNAL
#define FASTLED_ALLOW_INTERRUPTS 1
#define FASTLED_INTERRUPT_RETRY_COUNT 1
#include <FastLED.h>

#define LED_PIN1 13
#define LED_PIN2 12
#define LED_PIN3 11
#define LED_COUNT 144
CRGB *colors[3];
bool can_show = true;

// Need to add the ability to add and remove strips at anytime
// 	Implement a message system
//  Consider rewriting in C

void panic_if(bool cond) {
  if (cond) {
    while (true) {
      Serial.write("ERROR: NO MEM\n");
    }
  }
}

void setup()
{
  memset(colors, 0, sizeof(CRGB *) * 3);
  colors[0] = (CRGB *)malloc(sizeof(CRGB) * 144);
  colors[1] = (CRGB *)malloc(sizeof(CRGB) * 144);
  colors[2] = (CRGB *)malloc(sizeof(CRGB) * 144);
  panic_if(colors[0] == NULL);
  panic_if(colors[1] == NULL);
  panic_if(colors[2] == NULL);
  memset(colors[0], 0, sizeof(CRGB) * 144);
  memset(colors[1], 0, sizeof(CRGB) * 144);
  memset(colors[2], 0, sizeof(CRGB) * 144);
  FastLED.addLeds<WS2812, LED_PIN1, GRB>(colors[0], LED_COUNT);
  FastLED.addLeds<WS2812, LED_PIN2, GRB>(colors[1], LED_COUNT);
  FastLED.addLeds<WS2812, LED_PIN3, GRB>(colors[2], LED_COUNT);
  Serial.begin(2000000);
}

void update() {
  static uint32_t loc = 0;
  CRGB color;

  for (; loc < 432; loc++) {
    if (Serial.available() < 3) {
      return;
    }
    color.red = Serial.read();
    color.green = Serial.read();
    color.blue = Serial.read();
    if (loc < 144)
      colors[0][loc] = color;
    if (loc >= 144 && loc < 288)
      colors[1][loc - 144] = color;
    if (loc >= 288)
      colors[2][loc - 288] = color;
  }
  loc = 0;
  can_show = true;
}

void loop()
{
  update();
  if (can_show) {
    FastLED.show();
    can_show = false;
    Serial.println("");
  }
}
