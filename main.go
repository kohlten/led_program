package main

import (
	"gitlab.com/kohlten/led_program/internal"
	"flag"
)

var board = flag.String("board", "/dev/ttyACM0", "Board to write LEDs to")

func main() {
	flag.Parse()
	manager, err := internal.NewManager(*board, 2000000, 500)
 	if err != nil {
 		panic(err)
	}
	manager.AddStrip(internal.NewStrip(13, 144))
	manager.AddStrip(internal.NewStrip(12, 144))
	manager.AddStrip(internal.NewStrip(11, 144))
	for manager.IsRunning() {
		manager.Update()
	}
	manager.Close()
}
